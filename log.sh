#!/bin/bash

database_name=$1
key=$2

export GIT_DIR=databases/$database_name

git --no-pager log --graph --pretty='format:%ai - %h %aN <%ae> (%s)' HEAD -- $key