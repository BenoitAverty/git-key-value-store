#!/bin/bash

testSimpleTransaction() {
  ./use-transaction.sh $DB_NAME tx1

  echo "Some data" | ./put.sh $DB_NAME k1
  assertEquals "Some data" "$(./get.sh $DB_NAME k1)"

  ./commit-transaction.sh $DB_NAME tx1
  assertEquals "Some data" "$(./get.sh $DB_NAME k1)"
}
suite_addTest testSimpleTransaction

testRollback() {
  ./use-transaction.sh $DB_NAME tx1

  echo "Some data" | ./put.sh $DB_NAME k1
  assertEquals "Some data" "$(./get.sh $DB_NAME k1)"

  ./rollback-transaction.sh $DB_NAME tx1
  assertNull "$(./get.sh $DB_NAME k1 2> /dev/null)"
}
suite_addTest testRollback

testConcurrentTransactions() {
  # Write a value in tx1
  ./use-transaction.sh $DB_NAME tx1
  echo "Data in tx1" | ./put.sh $DB_NAME k1

  # Write a value in tx2 then commit
  ./use-transaction.sh $DB_NAME tx2
  echo "Data in tx2" | ./put.sh $DB_NAME k2
  ./commit-transaction.sh $DB_NAME

  # At that point we should see data from tx2 but not from tx1
  assertEquals "Data in tx2" "$(./get.sh $DB_NAME k2)"
  assertNull "$(./get.sh $DB_NAME k1 2> /dev/null)"

  # Commit tx1
  ./use-transaction.sh $DB_NAME tx1
  ./commit-transaction.sh $DB_NAME

  # Everything should be visible.
  assertEquals "Data in tx2" "$(./get.sh $DB_NAME k2)"
  assertEquals "Data in tx1" "$(./get.sh $DB_NAME k1)"
}
suite_addTest testConcurrentTransactions