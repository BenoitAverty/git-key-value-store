#!/bin/bash

testPutGet() {
  expected='Some data'
  
  echo 'Some data' | ./put.sh $DB_NAME k1

  actual=$(./get.sh $DB_NAME k1)

  assertEquals "$expected" "$actual"
}
suite_addTest testPutGet

testUpdate() {
  expected='New data'

  echo 'Some data' | ./put.sh $DB_NAME k1
  echo 'New data' | ./put.sh $DB_NAME k1

  actual=$(./get.sh $DB_NAME k1)

  assertEquals "$expected" "$actual"
}
suite_addTest testUpdate

testDelete() {
  echo 'Some data' | ./put.sh $DB_NAME k1
  ./delete.sh $DB_NAME k1

  actual=$(./get.sh $DB_NAME k1 2> /dev/null)

  assertNull "$actual"
}
suite_addTest testDelete
