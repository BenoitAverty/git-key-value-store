#!/bin/bash
# Call from the directory where the scripts are located

export DB_NAME=unit-tests-database

# Destroy and create a database before each test
setUp() {
  ./clear.sh $DB_NAME > /dev/null
  ./init.sh $DB_NAME > /dev/null
}

suite() {
  . $(dirname $0)/basic-crud.test.sh
  # Aliasing not reimplemented yet
  # . $(dirname $0)/aliasing.test.sh
  . $(dirname $0)/log.test.sh
  . $(dirname $0)/list.test.sh
  . $(dirname $0)/hierarchical.test.sh
  . $(dirname $0)/transactions.test.sh
}

. $(dirname $0)/shunit2/shunit2