

testAlias() {
  expected='Some data'
  
  echo 'Some data' | ./put.sh $DB_NAME k1
  ./alias.sh $DB_NAME alias k1

  actual=$(./get.sh $DB_NAME alias)

  assertEquals "$expected" "$actual"
}
suite_addTest testAlias

testAliasUpdate() {
  expected='New data'
  
  echo 'Some data' | ./put.sh $DB_NAME k1
  ./alias.sh $DB_NAME alias k1
  echo 'New data' | ./put.sh $DB_NAME k1

  actual=$(./get.sh $DB_NAME alias)

  assertEquals "$expected" "$actual"
}
suite_addTest testAliasUpdate

testAliasDelete() {  
  echo 'Some data' | ./put.sh $DB_NAME k1
  ./alias.sh $DB_NAME alias k1
  ./delete.sh $DB_NAME k1

  actual=$(./get.sh $DB_NAME alias)

  assertEquals '' "$actual"
}
suite_addTest testAliasDelete

testUpdatingAlias() {
  expected='New data'
  
  echo 'Some data' | ./put.sh $DB_NAME k1
  ./alias.sh $DB_NAME alias k1
  echo 'New data' | ./put.sh $DB_NAME alias

  actual1=$(./get.sh $DB_NAME alias)
  actual2=$(./get.sh $DB_NAME k1)

  assertEquals "$expected" "$actual1"
  assertEquals "$expected" "$actual2"
}
suite_addTest testUpdatingAlias

testDeletingAlias() {
  startSkipping # cannot delete lowercase symbolic ref... to investigate
  echo 'Some data' | ./put.sh $DB_NAME k1
  ./alias.sh $DB_NAME alias k1
  ./delete.sh $DB_NAME alias

  actual=$(./get.sh $DB_NAME alias)

  assertEquals '' "$actual"
}
suite_addTest testDeletingAlias