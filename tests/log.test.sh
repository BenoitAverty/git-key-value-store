#!/bin/bash

testLogKey() {  
  echo 'Some data' | ./put.sh $DB_NAME k1

  log_output=$(./log.sh $DB_NAME)
  actual=$(echo "$log_output" | wc -l)

  # 1 commit per action + init commit
  assertEquals 2 $actual
}
suite_addTest testLogKey

testLogKey2() {  
  echo 'Some data' | ./put.sh $DB_NAME k1
  echo 'Updated data' | ./put.sh $DB_NAME k1

  log_output=$(./log.sh $DB_NAME)
  actual=$(echo "$log_output" | wc -l)

  # 1 commit per action + init commit
  assertEquals 3 $actual
}

testLogOneKey() {
  echo 'Some data' | ./put.sh $DB_NAME k1
  echo 'Other data' | ./put.sh $DB_NAME k2

  log_output=$(./log.sh $DB_NAME k1)
  actual=$(echo "$log_output" | wc -l)

  # 1 commit per action
  assertEquals 1 $actual
}
suite_addTest testLogKey2