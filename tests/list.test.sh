testListAllKeys() {
  expected="k1
k2
k3"

  echo "some data" | ./put.sh $DB_NAME k1
  echo "some data" | ./put.sh $DB_NAME k2
  echo "some data" | ./put.sh $DB_NAME k3
  
  actual=$(./list.sh $DB_NAME)

  assertEquals "$expected" "$actual"
}
suite_addTest testListAllKeys