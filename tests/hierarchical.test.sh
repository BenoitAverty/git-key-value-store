#!/bin/bash

testHierarchical() {
  echo 'Some data' | ./put.sh $DB_NAME sub/key/yolo
  assertEquals "Some data" "$(./get.sh $DB_NAME sub/key/yolo)"

  echo 'Updated data' | ./put.sh $DB_NAME sub/key/yolo
  assertEquals "Updated data" "$(./get.sh $DB_NAME sub/key/yolo)"

  ./delete.sh $DB_NAME sub/key/yolo
  assertNull "$(./get.sh $DB_NAME sub/key/yolo 2> /dev/null)"
}
suite_addTest testHierarchical

testGetDirectory() {
  expected="tree HEAD:path/to

key1
key2"

  echo 'Some data' | ./put.sh $DB_NAME path/to/key1
  echo 'Some data' | ./put.sh $DB_NAME path/to/key2

  actual=$(./get.sh $DB_NAME path/to)

  assertEquals "$expected" "$actual"
}
suite_addTest testGetDirectory

testDeleteSubKey() {
  echo 'Some data' | ./put.sh $DB_NAME path/to/key1
  echo 'Some data' | ./put.sh $DB_NAME path/to/key2
  ./delete.sh $DB_NAME path/to/key2

  assertEquals "Some data" "$(./get.sh $DB_NAME path/to/key1)"
  assertNull "$(./get.sh $DB_NAME path/to/key2 2> /dev/null)"
}
suite_addTest testDeleteSubKey