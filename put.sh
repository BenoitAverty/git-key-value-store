#!/bin/bash

database_name=$1
key=$2

export GIT_DIR=databases/$database_name

# Store the current commit to use it as parent
parent_hash=$(git show-ref --head -s HEAD)

# Store the value in the objects db
object_hash=$(git hash-object -t blob -w --stdin < /dev/stdin)

# Create a tree that contains this object
git read-tree HEAD
git update-index --add --cacheinfo 100644,$object_hash,$key
tree_hash=$(git write-tree)

# Create a commit to track this tree
commit_hash=$(git commit-tree -m "Put $key" -p $parent_hash $tree_hash)

# Use the key to create a reference to that commit
git update-ref HEAD $commit_hash
