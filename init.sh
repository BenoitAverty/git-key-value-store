#!/bin/bash

database_name=$1

export GIT_DIR=databases/$database_name

git init --bare

# Make a first commit so that master points to something.
tree_hash=$(git write-tree)
commit_hash=$(git commit-tree -m"Init db $database_name" $tree_hash)
git update-ref HEAD $commit_hash