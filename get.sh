#!/bin/bash

database_name=$1
key=$2

export GIT_DIR=databases/$database_name

# Show the file called $key in the commit pointed by $key
git --no-pager show HEAD:$key