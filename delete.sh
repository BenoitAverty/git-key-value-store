#!/bin/bash

database_name=$1
key=$2

export GIT_DIR=databases/$database_name

# Store the current commit to use it as parent
parent_hash=$(git show-ref --head -s HEAD)

# Create a tree that contains this object
git read-tree HEAD
git rm --cached -- $key
tree_hash=$(git write-tree)

# Create a commit to track this tree
if [ -z "$parent_hash" ]; then
  commit_hash=$(git commit-tree -m "Delete $key" $tree_hash)
else
  commit_hash=$(git commit-tree -p $parent_hash -m "Delete $key" $tree_hash)
fi

# Use the key to create a reference to that commit
git update-ref HEAD $commit_hash