#!/bin/bash

database_name=$1
key=$2

export GIT_DIR=databases/$database_name

git read-tree HEAD
git ls-files --cached