#!/bin/bash

database_name=$1
tx_name=$2

export GIT_DIR=databases/$database_name

# Create the branch for the transaction (only if it doesn't exist)
git update-ref refs/heads/$tx_name refs/heads/master '' 2> /dev/null

# Use the transaction as current by storing it in HEAD
git symbolic-ref HEAD refs/heads/$tx_name
