#!/bin/bash

database_name=$1

export GIT_DIR=databases/$database_name

# Drop the current transaction
git update-ref -d HEAD

# Go back to master.
git symbolic-ref HEAD refs/heads/master
