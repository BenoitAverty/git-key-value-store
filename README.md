# Key-Value store implemented with git

## Inspirations

https://github.com/attic-labs/noms
https://www.kenneth-truyers.net/2016/10/13/git-nosql-database/

## Basic CRUD

tag : `basic-blob-and-ref`

We have : 
* Content deduplication with git

We're missing :
* History and metadata
* multiple branches (transactions)

### Bonus : aliasing

tag : `basic-blob-and-ref-with-aliasing`

Create aliases with symbolic refs. Updating the original key will update the alias.

## History

tag: `log-key-with-trees-and-commits`

We have : 
 * History and metadata per key
 * (todo) the ability to rollback

We're missing : 
 * transactions (refs are used as the keys)

## Transactions

tag: `transactions-with-branches`

We're now storing all values in the same tree, and refs become "branches". A branch is a transaction.

We have : 
 * Transactions with commit and rollback
 * long lived transactions
 * Hierarchical keys

We lose : 
 * Aliasing (refs do not point to blobs anymore)