#!/bin/bash

database_name=$1

export GIT_DIR=databases/$database_name
transaction_name=$(git symbolic-ref --short HEAD)
merge_worktree=databases/__worktrees/$database_name/$transaction_name

# Create a worktree in which the merge will be done.
git worktree add $merge_worktree master

# In the worktree, merge current transaction into master
(cd $merge_worktree && unset GIT_DIR && git merge --no-ff $transaction_name >/dev/null)

# Drop current transaction
git update-ref -d HEAD

# Go back to master
git symbolic-ref HEAD refs/heads/master

# Drop the now unnecessary worktree
git worktree remove $transaction_name